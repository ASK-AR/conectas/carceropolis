SHELL:=/bin/bash
UPDATE = FALSE
DATE_PREFIX = $(shell date +'%Y_%m_%d-%Hh')
DB_BKP_FILE := $(DATE_PREFIX)_dump.json
MEDIA_BKP := $(DATE_PREFIX)_media
CERTS_BKP := $(DATE_PREFIX)_certificates

.DEFAULT_GOAL : help

help:
	@echo "Welcome to Carcerópolis make file."
	@echo "This file is intended to ease your life regarding docker-compose commands."
	@echo "Bellow you will find the options you have with this makefile."
	@echo "You just need to run 'make <command> <arguments..>'."
	@echo "    "
	@echo "    help - Print this help message"
	@echo "    "
	@echo "    iniciar"
	@echo "        Inicializa o carcerópolis (docker swarm stack). Faz o build antes."
	@echo "    "
	@echo "    parar"
	@echo "        Para a stack do carcerópolis (sem limpar containers e volumes)."
	@echo "    "
	@echo "    atualizar"
	@echo "        Faz o build novamente e recarrega o projeto."
	@echo "    "
	@echo "    deploy"
	@echo "        Realiza o deploy do projeto."
	@echo "        ATENÇÃO: SÃO NECESSÁRIAS DIVERSAS VARIÁVEIS DE AMBIENTE PARA RODAR O PROJETO EM PRODUÇÃO."
	@echo "    "
	@echo "    build"
	@echo "        Faz o build  dos containers nginx e main."
	@echo "    "
	@echo "    migrations"
	@echo "        Gera as migrations para a versão atual do projeto."
	@echo "    "
	@echo "    geolocate [UPDATE=FALSE]"
	@echo "        Execute the gelolocation script for the Unidades Prisionais entities."
	@echo "        UPDATE: By default only Unidades without geolocation are updated, if you want to re-geolocate all Unidades, pass UPDATE=TRUE"
	@echo "    "
	@echo "    restore_latest_database_backup"
	@echo "        Restaura o backup do projeto mais recente. (Depende de ter os backups na pasta ../bkps)"
	@echo "    "
	@echo "    backup_database"
	@echo "        Dump the database to the '../bkps/db' directory preppended by the current date-time and compressed"
	@echo "    "
	@echo "    backup_media"
	@echo "        Copy media files to ../bkps/media directory and create a tar archive with it preppended with the current date-time"
	@echo "    "
	@echo "    backup_letsencrypt"
	@echo "        Faz backup dos certificados de segurança SSL em ../bkps/letsencrypt"
	@echo "    "
	@echo "    backup"
	@echo "        Run both backups (database and media)"
	@echo "    "
	@echo "    load_media"
	@echo "        Load media files into the carceropolis_media container"
	@echo "    "
	@echo "    load_letsencrypt"
	@echo "        Carrega o certificado de segurança mais recente dos backups."
	@echo "    "
.PHONY: help

iniciar: build
	@{ \
		if [[ "$$(docker info --format '{{.Swarm.LocalNodeState}}')" == "inactive" ]]; then \
    		docker swarm init --advertise-addr 127.0.0.1; \
		fi \
	}
	@docker stack up -c compose_dev.yml carceropolis
.PHONY: iniciar

parar:
	@docker stack down carceropolis
.PHONY: parar

atualizar: iniciar
.PHONY: atualizar

deploy:
	@{ \
		if [[ "$$(docker info --format '{{.Swarm.LocalNodeState}}')" == "inactive" ]]; then \
    		docker swarm init --advertise-addr 127.0.0.1; \
		fi \
	}
	@docker stack up -c docker-compose.yml carceropolis
.PHONY: deploy

build:
	@bash .gitlab.d/ci.sh build nginx
	@bash .gitlab.d/ci.sh build main
.PHONY: build

log_main:
	@docker service logs -f carceropolis_main
.PHONY: log_main

log_nginx:
	@docker service logs -f carceropolis_nginx
.PHONY: log_nginx

log_certs:
	@docker service logs -f carceropolis_letsencrypt
.PHONY: log_certs

migrations:
	docker exec $$(docker ps --filter name=carceropolis_main -q) python manage.py makemigrations
	mkdir -p tmp_migrations
	docker cp $$(docker ps --filter name=carceropolis_main --format '{{.Names}}'):/project/carceropolis/carceropolis/migrations/ tmp_migrations/
	find tmp_migrations/migrations -name '__pycache__' -or -name '*.pyc' -exec rm -rf {} \;
	cp -rf tmp_migrations/migrations/* carceropolis/carceropolis/migrations
	docker cp $$(docker ps --filter name=carceropolis_main --format '{{.Names}}'):/project/carceropolis/cidades/migrations/ tmp_migrations/
	find tmp_migrations/migrations -name '__pycache__' -or -name '*.pyc' -exec rm -rf {} \;
	cp -rf tmp_migrations/migrations/* carceropolis/cidades/migrations
	rm -rf tmp_migrations/migrations
.PHONY: migrations

geolocate:
ifeq ($(UPDATE), TRUE)
	docker exec $$(docker ps --filter name=carceropolis_main -q) python manage.py geolocate --all
else
	docker exec $$(docker ps --filter name=carceropolis_main -q) python manage.py geolocate
endif
.PHONY: geolocate

backup_database:
	@echo "Fazendo backup da base de dados do projeto (em ~/bkps/db/)"
	@bash .gitlab.d/backups.sh db
.PHONY: backup_database

backup_media:
	@echo "Fazendo backup da base de dados do projeto (em ~/bkps/media/)"
	@bash .gitlab.d/backups.sh media
.PHONY: backup_media

backup_letsencrypt:
	@echo "Fazendo backup da base de dados do projeto (em ~/bkps/letsencrypts/)"
	@bash .gitlab.d/backups.sh letsencrypt
.PHONY: backup_letsencrypt

backup: backup_database backup_media backup_letsencrypt
	@echo "Fazendo backup full do projeto (em ~/bkps/)"
	@bash .gitlab.d/backups.sh
.PHONY: backup

load_db:
	@echo "Carregando backup do banco de dados do projeto."
	@bash .gitlab.d/restore.sh db
.PHONY: load_letsencrypt

load_media:
	@echo "Carregando backup de arquivos de mídia no volume de mídia do projeto."
	@bash .gitlab.d/restore.sh media
.PHONY: load_media

load_letsencrypt:
	@echo "Carregando backup dos certificados de segurança do projeto."
	@bash .gitlab.d/restore.sh letsencrypt
.PHONY: load_letsencrypt
