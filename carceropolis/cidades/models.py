# coding: utf-8
#
# Thanks to: https://gist.github.com/JhonatasMartins/7eed599a2f95d005b81f
#
import json
import logging
from django.db import models
from django.core.exceptions import ObjectDoesNotExist

log = logging.getLogger("carceropolis")

STATE_CHOICES = (
    ('AC', 'Acre'), ('AL', 'Alagoas'), ('AP', 'Amapá'),
    ('AM', 'Amazonas'), ('BA', 'Bahia'), ('CE', 'Ceará'),
    ('DF', 'Distrito Federal'), ('ES', 'Espírito Santo'),
    ('GO', 'Goiás'), ('MA', 'Maranhão'), ('MT', 'Mato Grosso'),
    ('MS', 'Mato Grosso do Sul'), ('MG', 'Minas Gerais'),
    ('PA', 'Pará'), ('PB', 'Paraíba'), ('PR', 'Paraná'),
    ('PE', 'Pernambuco'), ('PI', 'Piauí'), ('RJ', 'Rio de Janeiro'),
    ('RN', 'Rio Grande do Norte'), ('RS', 'Rio Grande do Sul'),
    ('RO', 'Rondônia'), ('RR', 'Roraima'), ('SC', 'Santa Catarina'),
    ('SP', 'São Paulo'), ('SE', 'Sergipe'), ('TO', 'Tocantins')
)


class Cidade(models.Model):
    nome = models.CharField(max_length=60)
    estado = models.CharField(max_length=2, choices=STATE_CHOICES)

    def __str__(self):
        return f"{self.nome} ({self.estado})"

    @classmethod
    def __load_data__(cls, path):
        data = None
        adicionadas = []
        with open(path, "r") as data_file:
            data = json.load(data_file)

            for state in data.get("states"):
                uf = state.get("uf")
                for city in state.get("cities"):
                    try:
                        cidade = cls.objects.get(
                            estado=uf,
                            nome=city
                        )
                        log.info("Cidade %s/%s já existente", city, uf)
                    except ObjectDoesNotExist:
                        log.info("Criando nova cidade: %s/%s", city, uf)
                        cidade = cls()
                        cidade.uf = uf
                        cidade.nome = city
                        cidade.save()
                        adicionadas

        if len(adicionadas) > 0:
            print(f"{len(adicionadas)} cidades adicionadas.")
            print(", ".join(adicionadas))
        else:
            print("Nenhuma cidade adicionada.")
