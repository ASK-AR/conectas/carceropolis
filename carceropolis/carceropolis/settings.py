"""Main settings."""
import os
import sys
from pathlib import Path

IS_PRODUCTION = os.getenv("IS_PRODUCTION", "false").strip().lower() == "true"
DEBUG = os.getenv("DEBUG", "false").strip().lower() != "false"
LOG_LEVEL = os.getenv("CONSOLE_LOG_LEVEL", "ERROR")

#########
# PATHS #
#########
# Full filesystem path to the project.
PROJECT_APP_PATH = Path(__file__).resolve().parent
PROJECT_APP = str(PROJECT_APP_PATH.stem)
PROJECT_ROOT = BASE_DIR = PROJECT_APP_PATH.parent

######################
# MEZZANINE SETTINGS #
######################
# http://mezzanine.jupo.org/docs/configuration.html#default-settings

ADMIN_MENU_ORDER = (
    ("Carcerópolis Admin", (
        ("Áreas de Atuação", "caceropolis.AreaDeAtuacao"),
        ("Especialidades", "caceropolis.Especialidade"),
        ("Unidades Prisionais", "carceropolis.UnidadePrisional"),
        ("Logs", "django.contrib.admin.models.LogEntry"),
    )),
    ("Carcerópolis", (
        ("Especialistas", "caceropolis.Especialista"),
        ("Publicações", "caceropolis.Publicacao"),
    )),
    ("Conteúdos", (
        ("Páginas", "pages.Page"),
        ("Comentários", "generic.ThreadedComment"),
        ("Conteúdo de Mídia", "media-library"),
    )),
    ("Usuários", (
        ("Usuários", "auth.User"),
        ("Grupos", "auth.Group"),
    )),
    ("Sites", (
        ("Site", "sites.Site"),
        ("Redirecionamento", "redirects.Redirect"),
        ("Configurações", "conf.Setting"),
    )),
)

ADMIN_REMOVAL = ["django.contrib.sites.models.Site"]

MUNICIPIOS_GEO = False

# If True, the django-modeltranslation will be added to the
# INSTALLED_APPS setting.
USE_MODELTRANSLATION = True

########################
# MAIN DJANGO SETTINGS #
########################

ALLOWED_HOSTS = ["localhost", "127.0.0.1", "dev.carceropolis.org.br",
                 "carceropolis.org.br", "www.carceropolis.org.br"]

if IS_PRODUCTION:
    ALLOWED_HOSTS.insert(0, "carceropolis.org.br")
    CSRF_COOKIE_SECURE = True
    CSRF_COOKIE_SAMESITE = 'Strict'
    SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
    SESSION_COOKIE_SECURE = True

TIME_ZONE = "America/Sao_Paulo"
USE_TZ = True
USE_I18N = False
USE_L10N = False
LANGUAGE_CODE = "pt-br"
LANGUAGES = (
    ("pt-br", "Português Brasil"),
)

SESSION_EXPIRE_AT_BROWSER_CLOSE = True
SITE_ID = 1

AUTHENTICATION_BACKENDS = {
    "mezzanine.core.auth_backends.MezzanineBackend",
}

FILE_UPLOAD_PERMISSIONS = 0o644

STATIC_URL = "/static/"
STATIC_ROOT = "/static"

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)

MEDIA_URL = "/media/"
MEDIA_ROOT = "/carceropolis_media/"

ROOT_URLCONF = f"{PROJECT_APP}.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [str(PROJECT_ROOT / "templates")],
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "django.template.context_processors.i18n",
                "django.template.context_processors.static",
                "django.template.context_processors.media",
                "django.template.context_processors.tz",
                "mezzanine.conf.context_processors.settings",
                "mezzanine.pages.context_processors.page",
                "carceropolis.context_processors.add_login_form",
                "carceropolis.context_processors.add_registration_form",
                "carceropolis.context_processors.add_password_recover_form",
            ],
            "builtins": [
                "mezzanine.template.loader_tags",
            ],
            "loaders": [
                "mezzanine.template.loaders.host_themes.Loader",
                "django.template.loaders.filesystem.Loader",
                "django.template.loaders.app_directories.Loader"
            ]

        },
    },
]

################
# APPLICATIONS #
################

INSTALLED_APPS = (
    "mezzanine.blog",
    "cidades",
    "carceropolis",
    "django_extensions",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.postgres",
    "django.contrib.redirects",
    "django.contrib.sites",
    "django.contrib.sitemaps",
    "phonenumber_field",
    "mezzanine.boot",
    "mezzanine.conf",
    "mezzanine.core",
    "mezzanine.generic",
    "mezzanine.pages",
    "mezzanine.forms",
    "mezzanine.accounts",
    "logentry_admin",
    "compressor",
)

MIGRATION_MODULES = {
    "carceropolis": "carceropolis.migrations.carceropolis",
    "pages": "carceropolis.migrations.pages",
    "forms": "carceropolis.migrations.forms",
    "blog": "carceropolis.migrations.blog",
    "conf": "carceropolis.migrations.conf",
}

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",

    "mezzanine.core.request.CurrentRequestMiddleware",
    "mezzanine.core.middleware.RedirectFallbackMiddleware",
    "mezzanine.core.middleware.AdminLoginInterfaceSelectorMiddleware",
    "mezzanine.core.middleware.SitePermissionMiddleware",
    "mezzanine.pages.middleware.PageMiddleware",
]

PACKAGE_NAME_FILEBROWSER = "filebrowser_safe"
PACKAGE_NAME_GRAPPELLI = "grappelli_safe"

COMPRESS_ENABLED = True
COMPRESS_PRECOMPILERS = (
    ('text/x-scss', 'django_libsass.SassCompiler'),
)
LIBSASS_SOURCEMAPS = True

#########################
# OPTIONAL APPLICATIONS #
#########################

OPTIONAL_APPS = (
    "debug_toolbar",
    "django_extensions",
    PACKAGE_NAME_FILEBROWSER,
    PACKAGE_NAME_GRAPPELLI,
)

#########################
# CARCEROPOLIS SETTINGS #
#########################
PHONENUMBER_DB_FORMAT = "E164"
PHONENUMBER_DEFAULT_REGION = "BR"
BLOG_SLUG = "publicacoes"

LOG_FORMATTER = "verbose" if LOG_LEVEL in ("DEBUG", "INFO") else "simple"
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    'formatters': {
        'verbose': {
            'format': ('%(levelname)s %(asctime)s %(module)s %(process)d '
                       '%(thread)d %(message)s')
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "formatter": LOG_FORMATTER,
            "stream": sys.stdout,
        }
    },
    "loggers": {
        "carceropolis": {
            "handlers": ["console"],
            "level": LOG_LEVEL,
            "propagate": True,
        },
        "django": {
            "handlers": ["console"],
            "level": LOG_LEVEL,
            "propagate": True,
        }
    }
}

SECRET_KEY = os.getenv("SECRET_KEY") or "9b9n_5u!y-ge6+1+f##vt3ub9tp5hq(aq^4g&"
NEVERCACHE_KEY = os.getenv("NEVERCACHE_KEY") or "%vpsl!vjiw9m^4j(=c#gv47m849+t"

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": os.getenv("DB_NAME", "postgres"),
        "USER": os.getenv("DB_USER", "postgres"),
        "PASSWORD": os.getenv("DB_PASS", "carceropolis"),
        "HOST": os.getenv("DB_HOST", "db"),
    }
}

ACCOUNTS_APPROVAL_REQUIRED = True

ACCOUNTS_VERIFICATION_REQUIRED = True

try:
    PUBLICACAO_PER_PAGE = int(os.getenv("PUBLICACAO_PER_PAGE", 9))
except (ValueError, TypeError):
    PUBLICACAO_PER_PAGE = 9

# 2.5MB - 2621440
# 5MB - 5242880
# 10MB - 10485760
# 20MB - 20971520
# 50MB - 5242880
# 100MB 104857600
# 250MB - 214958080
# 500MB - 429916160
MAX_UPLOAD_SIZE = os.getenv("MAX_UPLOAD_SIZE", "5242880")

COMMENTS_DISQUS_SHORTNAME = False

ENABLE_COMMENTS = False

# Converting from PosixPath to str
PROJECT_APP_PATH = str(PROJECT_APP_PATH)
PROJECT_APP = str(PROJECT_APP)
PROJECT_ROOT = str(PROJECT_ROOT)

##################
# EMAIL SETTINGS #
##################
EMAIL_HOST = os.getenv("EMAIL_HOST")
EMAIL_HOST_USER = os.getenv("EMAIL_USER")
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
EMAIL_HOST_PASSWORD = os.getenv("EMAIL_PASSWORD")
EMAIL_PORT = str(os.getenv("EMAIL_PORT", "465"))
if EMAIL_PORT == "465":
    EMAIL_USE_SSL = True
    EMAIL_USE_TLS = False
elif EMAIL_PORT == "587":
    EMAIL_USE_SSL = False
    EMAIL_USE_TLS = True

os.environ['RECAPTCHA_DISABLE'] = 'True'
RECAPTCHA_PRIVATE_KEY = None
RECAPTCHA_PUBLIC_KEY = None

if IS_PRODUCTION:
    # First one on the list
    MIDDLEWARE.insert(0, "django.middleware.cache.UpdateCacheMiddleware")
    # Last one on the list
    MIDDLEWARE.append("django.middleware.cache.FetchFromCacheMiddleware")

    # Timeout here is the time that the django-server will hold the cached
    # files on the server, it is not directly related to the http headers
    # timeout information (defined below).
    CACHES = {
        "default": {
            "BACKEND": "django.core.cache.backends.memcached.MemcachedCache",
            "LOCATION": "memcache:11211",
            "TIMEOUT": 60*60*48
        }
    }

    CACHE_MIDDLEWARE_SECONDS = 60*60
    CACHE_MIDDLEWARE_KEY_PREFIX = PROJECT_APP
    STATICFILES_STORAGE = ("django.contrib.staticfiles.storage."
                           "ManifestStaticFilesStorage")

    # Recapthca:
    RECAPTCHA_PRIVATE_KEY = os.getenv("RECAPTCHA_PRIVATE_KEY")  # V2
    RECAPTCHA_PUBLIC_KEY = os.getenv("RECAPTCHA_PUBLIC_KEY")  # V2
    # RECAPTCHA_PRIVATE_KEY = os.getenv("RECAPTCHA_PRIVATE_KEY")  # V3
    # RECAPTCHA_PUBLIC_KEY = os.getenv("RECAPTCHA_PUBLIC_KEY")  # V3
    RECAPTCHA_DEFAULT_ACTION = 'generic'
    RECAPTCHA_SCORE_THRESHOLD = 0.5
    del os.environ['RECAPTCHA_DISABLE']

####################
# DYNAMIC SETTINGS #
####################
try:
    from mezzanine.utils.conf import set_dynamic_settings
except ImportError:
    pass
else:
    set_dynamic_settings(globals())
