#!/bin/bash
# vim: ai:ts=8:sw=8:noet

# Run this script only when in CI
if [[ "${GITLAB_CI:-false}" != "true" ]]; then
	echo "Esse script deve ser executado de apenas do CI."
	exit 42
fi

# Validando variáveis de ambiente necessárias:
while IFS=' ' read -r VARIAVEL SECRET; do
	if [[ "${!VARIAVEL:-undefined}" == "undefined" ]]; then
		echo "Você precisa definir a variável: ${VARIAVEL}"
		exit 42
	elif [[ "${SECRET}" == "FALSE" ]]; then
		echo " - ✔ ${VARIAVEL}: ${!VARIAVEL}"
	else
		echo " - ✔ ${VARIAVEL}: <<hidden>>"
	fi
done <<-EOF
	DB_HOST FALSE
	DB_NAME FALSE
	DB_PASSWORD TRUE
	DB_PORT FALSE
	DB_USER FALSE
	EMAIL_HOST FALSE
	EMAIL_PASSWORD TRUE
	EMAIL_PORT FALSE
	EMAIL_USER FALSE
	MAX_UPLOAD_SIZE FALSE
	NEVERCACHE_KEY FALSE
	PUBLICACAO_PER_PAGE FALSE
	RECAPTCHA_PUBLIC_KEY FALSE
	RECAPTCHA_PRIVATE_KEY FALSE
	REGISTRY FALSE
	SECRET_KEY TRUE
	SSH_PRIVATE_KEY TRUE
	SSH_PRODUCTION_HOST FALSE
	SSH_USER TRUE
	TAG FALSE
EOF

# Prepara chave ssh
function cleanup_ssh_key(){
	local STATUS=$?

	# Limpando pasta ssh temporária
	rm -rf "${HOME}/.ssh"
	# Restaurante pasta anterior caso ela exista
	if [[ -d "${HOME}/ssh_bkp" ]]; then
	    mv "${HOME}/.ssh_bkp" "${HOME}/.ssh"
	fi

	exit ${STATUS}
}

# Faz um backup da pasta ${HOME}/.ssh, caso exista
if [[ -d "${HOME}/.ssh" ]]; then
    mv "${HOME}/.ssh" "${HOME}/.ssh_bkp"
fi

# Estou adicionando isso aqui para garantir que em qualquer hipótese nós vamos
# limpar a pasta ssh do runner, para evitar vazar nossa chave ssh privada.
trap cleanup_ssh_key EXIT

# Garantindo que o agent ssh está "conectado" à nossa sessão
eval "$(ssh-agent -s)"

# Adicionando nossa chave privada ao agent ssh de forma que não vamos vazar ele
# no console
echo "${SSH_PRIVATE_KEY}" | base64 -d | tr -d '\r' | ssh-add -

mkdir -p "${HOME}/.ssh"

chmod 700 "${HOME}/.ssh"

# Adiciona chave ssh do servidor para o chaveiro como uma chave conhecida.
ssh-keyscan "${SSH_PRODUCTION_HOST}" >> "${HOME}/.ssh/known_hosts"

chmod 644 "${HOME}/.ssh/known_hosts"

echo "# "
echo "########################################################################"
echo "# Logging into the server"
echo "# "

# Agora sim rodando o deploy lá no servidor do radar
set +u
# shellcheck disable=SC2087
ssh -T "${SSH_USER}@${SSH_PRODUCTION_HOST}" <<-CODE
	echo "Logado no servidor."
	set -eufCo pipefail
	echo "# "
	echo "#####################"
	# Disabling shell history
	set +uo history
	if [[ ! -d "/home/ubuntu/carceropolis" ]]; then
		echo "Clonando o projeto localmente."
		git clone "${CI_PROJECT_URL}.git" "/home/ubuntu/carceropolis"
		cd "/home/ubuntu/carceropolis"
		echo "Criando e populando o volume de arquivos de mídia"
		docker volume create carceropolis_media
		make load_media
		echo "Criando e populando volume de certificados SSL."
		docker volume create carceropolis_letsencrypt
		make load_letsencrypt
		echo "Deixando indicado de que há necessidade de load do bkp da base de dados."
		touch .restore_database
	fi
	cd "/home/ubuntu/carceropolis"
	echo "Configurando variáveis de ambiente"
	export CONSOLE_LOG_LEVEL=WARN
	export DB_HOST="${DB_HOST}"
	export DB_NAME="${DB_NAME}"
	export DB_PASSWORD="${DB_PASSWORD}"
	export DB_PORT="${DB_PORT}"
	export DB_USER="${DB_USER}"
	export DEBUG=False
	export DOCKER_REGISTRY="${REGISTRY}"
	export EMAIL_HOST="${EMAIL_HOST}"
	export EMAIL_PASSWORD="${EMAIL_PASSWORD}"
	export EMAIL_PORT="${EMAIL_PORT}"
	export EMAIL_USER="${EMAIL_USER}"
	export HTTP_PORT="${HTTP_PORT:-80}"
	export HTTPS_PORT="${HTTPS_PORT:-443}"
	export IS_PRODUCTION=True
	export MAX_UPLOAD_SIZE="${MAX_UPLOAD_SIZE}"
	export NEVERCACHE_KEY="$(echo "${NEVERCACHE_KEY}" | base64 -d)"
	export PUBLICACAO_PER_PAGE="${PUBLICACAO_PER_PAGE}"
	export RECAPTCHA_PRIVATE_KEY="${RECAPTCHA_PRIVATE_KEY}"
	export RECAPTCHA_PUBLIC_KEY="${RECAPTCHA_PUBLIC_KEY}"
	export SECRET_KEY="$(echo "${SECRET_KEY}" | base64 -d)"
	export TAG="${TAG}"
	git fetch --all --prune --tags
	# Limpando quaisquer mudanças que tenham sido feitas manualmente.
	git reset --hard HEAD
	# Baixando a última versão do código do projeto
	echo "# "
	echo "# Baixando a última versão"
	git checkout "${CI_COMMIT_SHA}"
	echo "# "
	echo "# Atualizando o serviço"
	# Atualizando a stack do radar
	docker stack deploy -c docker-compose.yml carceropolis
	# Recarrega o nginx para usar o certificado atualizado, caso o
	# letsencrypt tenha atualizado o certificado.
	sleep 5
	echo "Forçando o restart do nginx."
	docker service update --force carceropolis_nginx
	# Fazendo algumas limpezas
	docker container prune -f
	sleep 5
	docker image prune -a -f
	# Restore database if not yet restored
	if [[ -f ".restore_database" ]]; then
		rm .restore_database
		make restore_latest_database_backup
	fi
	# Reenable shell history
	set -o history
CODE

# TODO: Validar o endpoint de versão do radar.
echo "Implantação realizada, agora é ver se está tudo ok!"
