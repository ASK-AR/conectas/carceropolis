#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# Gera a maioria das variáveis importantes que precisamos, diferenciando se
# estamos rodando no CI ou não.
set -eufCo pipefail
export SHELLOPTS	# propagate set to children by default
IFS=$'\t\n'

#################################################################
# Variáveis gerais
REGISTRY="${CI_REGISTRY_IMAGE:-registry.gitlab.com/ask-ar/conectas/carceropolis}"
# Referências de Diretórios
INIT_DIR="${PWD}"
ROOT_DIR="$(git rev-parse --show-toplevel)"
DOCKERFILES="${ROOT_DIR}/dockerfiles"

function _cmd_check() {
	cmd="$1"
	command -v "${cmd}" >/dev/null 2>&1 \
		|| { echo "${cmd} is required and is not installed, exiting!"; \
			exit 42; }
}

function cmd_checks() {
	_cmd_check "git"
	_cmd_check "bash"
}

function cleanup() {
	local STATUS=$?
	cd "${INIT_DIR}"
	exit ${STATUS}
}

function ci_only() {
	if [[ "true" != "${GITLAB_CI:-false}" ]]; then
		echo "Rodando script de CI fora do CI, terminando."
		exit 42
	fi
}

# Gearante uma limpeza no final da execução.
# shellcheck disable=SC1090
trap cleanup EXIT

# Checando existência de alguns programas necessários
cmd_checks

FUNCAO=$1
# Checando se foram passados dois argumentos
# Esperado (deploy)((build|release) (nginx|main))
if [[ "${FUNCAO}" =~ ^(build|release)$ ]]; then
	if [[ ! "$2" =~ ^(nginx|main)$ ]]; then
		echo "Wrong ${FUNCAO} option $2"
		exit 42
	fi
	IMAGEM=$2

	# Referências pra nome das imagens docker
	DOCKERFILE="${DOCKERFILES}/${IMAGEM}"
	IMAGEM_DOCKER="${REGISTRY}/${IMAGEM}"
elif [[ "${FUNCAO}" != "deploy" ]]; then
	echo "Invalid argument ${FUNCAO}"
	exit 42
fi

TAG="dev-local"

if [[ "true" == "${GITLAB_CI:-false}" ]]; then
	# Login in gitlab docker registry
	echo "${CI_JOB_TOKEN}" \
		| \
		docker login \
		-u gitlab-ci-token \
		--password-stdin \
		"${CI_REGISTRY}"

	# Define a TAG a ser utilizada
	if [[ "${CI_COMMIT_REF_NAME}" == "master" ]]; then
		# Se estivermos na branch master, usamos o hash do commit.
		TAG="${CI_COMMIT_SHORT_SHA}"
	else
		# Do contrário usamos o nome da branch
		TAG="${CI_COMMIT_REF_NAME//\//_}"
	fi
fi

cd "${ROOT_DIR}"
case ${FUNCAO} in
	build)
		docker build \
		    --cache-from "${IMAGEM_DOCKER}:${TAG}" \
		    --cache-from "${IMAGEM_DOCKER}:latest" \
		    -t "${IMAGEM_DOCKER}:${TAG}" \
		    -f "${DOCKERFILE}" \
		.
		;;
	release)
		docker build \
		    --cache-from "${IMAGEM_DOCKER}:${TAG}" \
		    --cache-from "${IMAGEM_DOCKER}:latest" \
		    -t "${IMAGEM_DOCKER}:${TAG}" \
		    -t "${IMAGEM_DOCKER}:latest" \
		    -f "${DOCKERFILE}" \
		.

		docker push "${IMAGEM_DOCKER}:${TAG}"
		docker push "${IMAGEM_DOCKER}:latest"
		;;
	deploy)
		# shellcheck disable=SC1090
		source "${ROOT_DIR}/.gitlab.d/deploy.sh"
		;;
	*)
		exit 42
		;;
esac

echo
echo "Ok, tudo certo, encerrando por aqui!"
