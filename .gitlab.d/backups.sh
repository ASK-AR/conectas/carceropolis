#!/bin/bash
# vim: ai:ts=8:sw=8:noet
set -xeufCo pipefail
export SHELLOPTS	# propagate set to children by default
IFS=$'\t\n'

LOCAL_BKP_PATH="${2:-${HOME}/bkps}"
PREFIX="$(date +'%Y_%m_%d-%Hh%Mm')"
mkdir -p "${LOCAL_BKP_PATH}/"{db,letsencrypt,media}

function backup_db(){
	# Now let's make the copy of the docker volumes to "${HOME}/bkps/"
	docker run --rm \
	    -v carceropolis_db:/bkp_db \
	    -v "${LOCAL_BKP_PATH}":/local_bkp \
	    -i \
	    debian:stable-slim \
	    /bin/bash -s <<-EOF
		set -x
		tar czf "/local_bkp/db/${PREFIX}_db.tar.gz" /bkp_db
EOF
}

function backup_letsencrypt(){
	# Now let's make the copy of the docker volumes to "${HOME}/bkps/"
	docker run --rm \
	    -v carceropolis_letsencrypt_conf:/bkp_letsencrypt_conf \
	    -v "${LOCAL_BKP_PATH}":/local_bkp \
	    -i \
	    debian:stable-slim \
	    /bin/bash -s <<-EOF
		set -x
		tar czf "/local_bkp/letsencrypt/${PREFIX}_letsencrypt_conf.tar.gz" /bkp_letsencrypt_conf
EOF
}

function backup_media(){
	# Now let's make the copy of the docker volumes to "${HOME}/bkps/"
	docker run --rm \
	    -v carceropolis_media:/bkp_media \
	    -v "${LOCAL_BKP_PATH}":/local_bkp \
	    -i \
	    debian:stable-slim \
	    /bin/bash -s <<-EOF
		set -x
		tar cf "/local_bkp/media/${PREFIX}_media.tar" /bkp_media
EOF
}

case "${1:-all}" in
	db)
		# First scale all services down to avoid concurrency issues
		docker service scale carceropolis_main=0 carceropolis_db=0
		backup_db
		# Bring the containers up again
		docker service scale carceropolis_db=1 carceropolis_main=1
		;;
	letsencrypt | certs)
		backup_letsencrypt
		;;
	media)
		# First scale all services down to avoid concurrency issues
		docker service scale carceropolis_main=0 carceropolis_nginx=0
		backup_media
		# Bring the containers up again
		docker service scale carceropolis_nginx=1 carceropolis_main=1
		;;
	*)
		# First scale all services down to avoid concurrency issues
		docker service scale carceropolis_db=0 carceropolis_main=0 carceropolis_nginx=0
		backup_db
		docker service scale -d carceropolis_db=1
		backup_media
		# Bring the containers up again
		docker service scale carceropolis_db=1 carceropolis_main=1 carceropolis_nginx=1
		backup_letsencrypt
		;;
esac
