#!/bin/bash
# vim: ai:ts=8:sw=8:noet
set -xeufCo pipefail
export SHELLOPTS	# propagate set to children by default
IFS=$'\t\n'

LOCAL_BKP_PATH="${2:-${HOME}/bkps}"
if [[ ! -d "${LOCAL_BKP_PATH}" ]]; then
	echo "Não há backups em ${LOCAL_BKP_PATH}"
	exit 1
fi

function restore_db(){
	# Now let's make the copy of the docker volumes to "${HOME}/bkps/"
	docker run --rm \
	    -v carceropolis_db:/bkp_db \
	    -v "${LOCAL_BKP_PATH}":/local_bkp \
	    -i \
	    debian:stable-slim \
	    /bin/bash -s <<-EOF
		set -x
		find /local_bkp/db/ -maxdepth 1 -type f -name "*.tar.gz" \
			| sort \
			| tail -n 1 \
			| xargs -I{} tar xzf {}
EOF
}

function restore_letsencrypt(){
	# Now let's make the copy of the docker volumes to "${HOME}/bkps/"
	docker run --rm \
	    -v carceropolis_letsencrypt_conf:/bkp_letsencrypt_conf \
	    -v "${LOCAL_BKP_PATH}":/local_bkp \
	    -i \
	    debian:stable-slim \
	    /bin/bash -s <<-EOF
		set -x
		find /local_bkp/letsencrypt/ -maxdepth 1 -type f -name "*.tar.gz" \
			| sort \
			| tail -n 1 \
			| xargs -I{} tar xzf {} --keep-newer-files
EOF
}

function restore_media(){
	# Now let's make the copy of the docker volumes to "${HOME}/bkps/"
	docker run --rm \
	    -v carceropolis_media:/bkp_media \
	    -v "${LOCAL_BKP_PATH}":/local_bkp \
	    -i \
	    debian:stable-slim \
	    /bin/bash -s <<-EOF
		set -x
		find /local_bkp/letsencrypt/ -maxdepth 1 -type f -name "*.tar" \
			| sort \
			| tail -n 1 \
			| xargs -I{} tar xzf {} --keep-newer-files
EOF
}

case "${1:-all}" in
	db)
		# First scale all services down to avoid concurrency issues
		docker service scale carceropolis_main=0 carceropolis_db=0
		restore_db
		# Bring the containers up again
		docker service scale carceropolis_db=1 carceropolis_main=1
		;;
	letsencrypt | certs)
		restore_letsencrypt
		# Restart nginx to get the new certificate
		docker service update --force carceropolis_nginx
		;;
	media)
		restore_media
		;;
	*)
		# First scale all services down to avoid concurrency issues
		docker service scale carceropolis_db=0 carceropolis_main=0
		restore_db
		# Bring the containers up again
		docker service scale -d carceropolis_db=1 carceropolis_main=1
		restore_letsencrypt
		# Restart nginx to get the new certificate
		docker service update --force carceropolis_nginx=1
		restore_media
		;;
esac
