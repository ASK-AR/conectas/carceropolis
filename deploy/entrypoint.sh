#!/usr/bin/env bash
TRY_LOOP="20"

wait_for_port() {
  local name="$1" host="$2" port="$3"
  local j=0
  while ! nc -z "$host" "$port" >/dev/null 2>&1 < /dev/null; do
    j=$((j+1))
    if [ $j -ge $TRY_LOOP ]; then
      echo >&2 "$(date) - $host:$port still not reachable, giving up"
      exit 1
    fi
    echo "$(date) - waiting for $name ($host:$port)... $j/$TRY_LOOP"
    sleep 5
  done
}

wait_for_db() {
    local host=${DB_HOST:-db}
    local port=${DB_PORT:-5432}

    wait_for_port "Postgres" "$host" "$port"
}

wait_for_db

echo "  Running migrations."
python manage.py migrate --no-input
echo "  Compiling translated messages"
python manage.py compilemessages
echo "  Collecting static files."
python manage.py collectstatic --noinput
echo "  Compressing files static"
python manage.py compress --force
echo "Starting uwsgi"
uwsgi --ini /project/deploy/uwsgi.ini
