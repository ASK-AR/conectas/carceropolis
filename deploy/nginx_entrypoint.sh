#!/bin/sh

LIVE_PATH="/letsencrypt/live/carceropolis.org.br"
mkdir -p "${LIVE_PATH}"
if [ ! -f "${LIVE_PATH}/privkey.pem" ]; then
    ln -s /selfsignedcert/self_signed_privkey.pem "${LIVE_PATH}/privkey.pem"
    ln -s /selfsignedcert/self_signed_fullchain.pem "${LIVE_PATH}/fullchain.pem"
fi

# Defaul command do container do nginx
nginx -g "daemon off;"
