# Carcerópolis

O Brasil é um dos países que mais prende no mundo. Carcerópolis é um projeto
que reúne dados e pesquisas que ajudam a revelar a estrutura e a população dessa
nação atrás das grades.

Organização: [Associação CONECTAS Direitos Humanos]

Desenvolvimento e Consultoria de Dados: [ASK-AR]

Consultoria jurídica: Thandara Santos e Paula R. Ballesteros.

Veja a [Documentação] do projeto caso queira desenvolver ou rodar o projeto.

# Certificado SSL/HTTPS

Em teoria, a renovação do certificado de segurança SSL/HTTPS gerado pelo letsencrypt é periódica e automática.
Podem, caso ela falhe, uma forma de rodar novamente o certbot é executando os comandos:

```
docker service scale carceropolis_letsencrypt=0
docker service scale carceropolis_letsencrypt=1
docker service update --force carceropolis_nginx
```

Antes de reiniciar o nginx vale olhar os logs do letsencrypt para ter certeza que tudo deu certo:
- `docker service logs carceropolis_letsencrypt`

Se for necessária alguma intervenção manual dos arquivos do letsencrypt pode-se fazer via:
- `docker run --rm -it -v carceropolis_letsencrypt_conf:/etc/letsencrypt --entrypoint sh certbot/certbot`

[ASK-AR]: https://ask-ar.xyz
[Associação CONECTAS Direitos Humanos]: http://conectas.org/
[Documentação]: https://gitlab.com/ASK-AR/conectas/carceropolis/-/blob/master/CONTRIBUTING.md
