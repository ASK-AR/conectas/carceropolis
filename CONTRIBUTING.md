# CONTRIBUINDO COM O PROJETO

## Desenvolvimento e Dados

Para quem quiser contribuir com o desenvolvimento do projeto, seja a plataforma
seja no tratamento e análise de dados, aqui estão as principais guidelines que
você precisa saber:

### Tecnologias

#### Dados

Sob a perspectiva de dados, as fontes de dados são as bases brutas do Infopen
fornecidas pelo Ministério da Justiça.....

TODO: COMPLETAR

#### Plataforma

A Plataforma Carcerópolis foi desenvolvida usando o CMS [mezzanine], construído
sob o framework Django, que usa a linguagem de programação Python. Além disso,
utilizamos também o pacote Bokeh para geração de gráficos.

Todo o projeto foi desenhado para funcionar em containers Docker, de forma que a
pessoa que deseja rodar o projeto, seja para desenvolvimento, seja para
produção, precisa ter instalado em sua máquina local o **docker** e o terminal
**bash**.

Para facilitar alguns processos, há ainda um Makefile orientado para
desenvolvimento local e também para algumas tarefas pontuais (como backups e
carga inicial de dados).

`make help` é o seu melhor amigo e te mostrará como usar o Makefile (você
precisa ter o `make` instalado também, claro).

Sob a perspectiva de desenvolvimento, o projeto usa ainda SASS/SCSS, mas a
compilação do mesmo é realizada pelo próprio Django. Os arquivos SASS/SCSS do
projeto estão na pasta `carceropolis/carceropolis/static/scss`.

<!-- Seção que precisa ser revista
## Traduções

Para traduzir algum texto, primeiro adicione o que quer traduzir com `{% trans
"palavra ou id" %}`, depois gere os `.po`:

    python manage.py makemessages -l en -l pt_BR

Depois edite:

    carceropolis/carceropolis/locale/en/LC_MESSAGES/django.po
    carceropolis/carceropolis/locale/pt_BR/LC_MESSAGES/django.po

E por fim compile:

    python manage.py compilemessages

Mais informações aqui: https://docs.djangoproject.com/en/2.0/topics/i18n/translation/
-->

Você precisará definir, em seu ambiente, uma ou mais das seguintes variáveis:

- `DB_HOST`
- `DB_NAME`
- `DB_PASSWORD`
- `DB_PORT`
- `DB_USER`
- `CONSOLE_LOG_LEVEL`
- `EMAIL_HOST`
- `EMAIL_PASSWORD`
- `EMAIL_PORT`
- `EMAIL_USER`
- `HTTP_PORT`
- `HTTPS_PORT`
- `IS_PRODUCTION`
- `MAX_UPLOAD_SIZE`
- `NEVERCACHE_KEY`
- `PUBLICACAO_PER_PAGE`
- `SECRET_KEY`

Mais informações sobre estas variáveis podem ser encontradas nos arquivos:

- `compose_dev.yml`; e
- `carceropolis/carceropolis/settings.py`.

## Produção

O projeto foi desenhado para que seja colocado em produção utilizando o CI/CD do
[gitlab]. Para tanto, temos o arquivo de configuração do CI `.gitlab-ci.yml` e
também os scripts no diretório `.gitlab.d/`.

As variáveis de ambientes necessárias estão configuradas nas configurações de
CI/CD do próprio repositório no gitlab ([veja aqui][1]).

O CI/CD fará todo o trabalho de deploy contínuo, exceto o primeiro por depender
do load inicial dos dados (depende de backups anteriores).

Além disso, o projeto também está configurado para fazer a emissão de
certificado HTTPS/SSL usando o letsencrypt/certbot automaticamente.

As variáveis usadas no CI são:

- `DB_HOST`
- `DB_NAME`
- `DB_PASSWORD`
- `DB_PORT`
- `DB_USER`
- `EMAIL_HOST`
- `EMAIL_PASSWORD`
- `EMAIL_PORT`
- `EMAIL_USER`
- `HTTP_PORT`
- `HTTPS_PORT`
- `MAX_UPLOAD_SIZE`
- `NEVERCACHE_KEY` (base64 encoded)
- `PRODUCTION_HOST`
- `PUBLICACAO_PER_PAGE`
- `SECRET_KEY` (base64 encoded)
- `SSH_PRIVATE_KEY` (base64 encoded)
- `SSH_PRODUCTION_HOST`
- `SSH_USER`

Mais informações sobre estas variáveis podem ser encontradas nos arquivos:

- `docker-compose.yml`; e
- `carceropolis/carceropolis/settings.py`.

[1]: https://gitlab.com/ASK-AR/conectas/carceropolis/-/settings/ci_cd
[mezzanine]: http://mezzanine.jupo.org/
[gitlab]: https://gitlab.com
