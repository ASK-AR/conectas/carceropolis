#!/bin/bash

command -v sed || { echo "Você precisa instalar o software 'sed' antes de rodar este script."; exit 42; }
command -v iconv || { echo "Você precisa instalar o software 'iconv' antes de rodar este script."; exit 42; }
command -v dos2unix || { echo "Você precisa instalar o software 'dos2unix' antes de rodar este script."; exit 42; }

if [[ "$#" -ne 1 || ! -f "$1" ]]; then
    echo "Você precisa passar o nome do arquivo que deseja alterar.";
    exit 1
fi

ARQUIVO="$1"

# Converte de ISO8859-1 para UTF-8
if [[ "$(file -i "${ARQUIVO}" | awk '{ print $3 }')" != "charset=utf-8" ]]; then
    iconv -f ISO-8859-1 -t UTF-8 "${ARQUIVO}" > "${ARQUIVO}_utf8"
    mv "${ARQUIVO}_utf8" "${ARQUIVO}"
fi

# Converte arquivo de DOS para UNIX
# Faz ajustes de padrão de quebra de linha e alguns caracteres como o BOM (Byte
# order mark)
dos2unix -r "${ARQUIVO}"

# Removendo caracteres de espaço não padrões.
#
# Existem diversos tipos de "espaços", que são representados por códigos distintos.
#
# Quando comparamos strings, eles não são equivalentes, mas não
# conseguimos identificá-los visualmente (pois todos são espaços). Por
# isso realizei uma substituição completa nas bases originais de um desses
# espaços que eu identifiquei (representado na tabela UTF8 pelo caracter
# \xC2\xA0).
#
# Ref 1: https://en.wikipedia.org/wiki/Non-breaking_space
# Ref 2: https://superuser.com/questions/517847/use-sed-to-replace-nbsp-160-hex-00a0-octal-240-non-breaking-space
sed -i 's/\xC2\xA0/ /g' "${ARQUIVO}"
