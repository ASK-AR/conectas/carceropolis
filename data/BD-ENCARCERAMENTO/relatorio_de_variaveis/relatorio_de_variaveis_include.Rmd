```{r, echo=FALSE, include=FALSE}
# Este arquivo não foi criado para ser executado,
# Ele é incluído no arquivo "Relatório_de_variaveis.Rmd"
# de forma automatica
```
## {{variavel}}

As categorias esperadas são:

```{r, echo=FALSE, results='markup', null_prefix=TRUE}
for (level in levels_finais[["{{variavel}}"]]) {
  print(paste0("  - ", '"', level, '"'), quote = FALSE)
}
```

Porém, os seguintes valores não foram possíveis de serem adequados à lista acima:

```{r, echo=FALSE, results='markup', null_prefix=TRUE}
trim <- function (x) gsub("^\\s+|\\s+$", "", x)
for (valor_errado in correcoes[["{{variavel}}"]]) {
  print(paste0("  - ", '"', valor_errado, '" (', sum(infopen[,trim("{{variavel}}")]==valor_errado), ')'), quote=FALSE)
}
```