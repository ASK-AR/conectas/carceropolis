library(dplyr)

# Extraindo os dados referentes a unidades prisionais de 2014-dezembro para um csv a parte
read.csv2("infopen_2014_06.csv", stringsAsFactors = FALSE) %>%
  mutate(id_unidade="",
         UF=gsub(".*[(]([[:upper:]][[:upper:]])[)]", "\\1", UF)) %>%
  rename(nome_unidade="Nome.da.unidade.prisional.", endereco="Endereço.da.Unidade.", cep="CEP.", uf=UF, municipio=Cidade, telefone="Telefone.para.população.obter.informações.sobre.visitação.") %>%
  select(id_unidade, nome_unidade, endereco, cep, uf, municipio, telefone) %>%
  write.csv2(file="unidades_2014_06.csv", fileEncoding="UTF-8", quote=FALSE)

# Extraindo os dados referentes a unidades prisionais de 2014-dezembro para um csv a parte
read.csv2("infopen_2014_12.csv", stringASFactors=FALSE) %>%
  mutate(id_unidade=row_number()) %>%
  rename(nome_unidade="Nome.da.unidade.prisional.", endereco="Endereço.da.Unidade.", cep="CEP.", uf=UF, municipio=Município, telefone="Telefone.para.população.obter.informações.sobre.visitação.") %>%
  select(id_unidade, nome_unidade, endereco, cep, uf, municipio, telefone) %>%
  write.csv2(file="unidades_2014_12.csv", fileEncoding="UTF-8", quote=FALSE)

# Extraindo os dados referentes a unidades prisionais de 2015 para um csv a parte
read.csv2("infopen_2015_12.csv", stringsAsFactors=FALSE) %>%
  mutate(id_unidade="") %>%
  rename(nome_unidade="Nome.da.unidade.prisional.", endereco="Endereço.da.Unidade.", cep="CEP.", uf=UF, municipio="Município", telefone="Telefone.para.população.obter.informações.sobre.visitação.") %>%
  select(id_unidade, nome_unidade, endereco, cep, uf, municipio, telefone) %>%
  write.csv2(file="unidades_2015_12.csv", fileEncoding="UTF-8", quote=FALSE)

# Extraindo os dados referentes a unidades prisionais de junho de 2016 para um csv a parte
read.csv2("infopen_2016_06.csv", stringsAsFactors=FALSE) %>%
  mutate(id_unidade="") %>%
  rename(nome_unidade="Nome.da.unidade.prisional.", endereco="Endereço.da.Unidade.", cep="CEP.", uf=UF, municipio="Município", telefone="Telefone.para.população.obter.informações.sobre.visitação.") %>%
  select(id_unidade, nome_unidade, endereco, cep, uf, municipio, telefone) %>%
  write.csv2(file="unidades_2016_06.csv", fileEncoding="UTF-8", quote=FALSE)

# Extraindo os dados referentes a unidades prisionais de 2016 para um csv a parte
read.csv2("infopen_2016_12.csv", stringsAsFactors=FALSE) %>%
  mutate(id_unidade="") %>%
  rename(nome_unidade="Nome.da.unidade.prisional.", endereco="Endereço.da.Unidade.", cep="CEP", uf=UF, municipio="Município", telefone="Telefone.Principal") %>%
  select(id_unidade, nome_unidade, endereco, cep, uf, municipio, telefone) %>%
  write.csv2(file="unidades_2016_12.csv", fileEncoding="UTF-8", quote=FALSE)

# Extraindo os dados referentes a unidades prisionais de jun de 2017 para um csv a parte
read.csv2("infopen_2017_06.csv", stringsAsFactors=FALSE) %>%
  mutate(id_unidade="") %>%
  rename(nome_unidade="Nome.da.unidade.prisional.", endereco="Endereço.da.Unidade.", cep="CEP.", uf=UF, municipio="Município", telefone="Telefone.para.população.obter.informações.sobre.visitação.") %>%
  select(id_unidade, nome_unidade, endereco, cep, uf, municipio, telefone) %>%
  write.csv2(file="unidades_2017_06.csv", fileEncoding="UTF-8", quote=FALSE)

# Extraindo os dados referentes a unidades prisionais de dez de 2017 para um csv a parte
read.csv2(file= "infopen_2017_12.csv", stringsAsFactors = FALSE) %>%
  mutate(id_unidade=NA) %>%
  rename(nome_unidade="Nome.do.Estabelecimento", endereco="Endereço", municipio="Município", cep="CEP", uf="UF", telefone="Telefone.Principal") %>%
  select(id_unidade, nome_unidade, endereco, cep, uf, municipio, telefone) %>%
  write.csv2(file = "unidades_2017_12.csv", na = "", fileEncoding = "UTF-8")

# Extraindo os dados referentes a unidades prisionais de jun de 2018 para um csv a parte
read.csv2(file= "infopen_2018_06.csv", stringsAsFactors = FALSE) %>%
  mutate(id_unidade=NA) %>%
  rename(nome_unidade="Nome.do.Estabelecimento", endereco="Endereço", municipio="Município", cep="CEP", uf="UF", telefone="Telefone.Principal") %>%
  select(id_unidade, nome_unidade, endereco, cep, uf, municipio, telefone) %>%
  write.csv2(file = "unidades_2018_06.csv", na = "", fileEncoding = "UTF-8")

# Extraindo os dados referentes a unidades prisionais de dez de 2018 para um csv a parte
read.csv2(file= "infopen_2018_12.csv", stringsAsFactors = FALSE) %>%
  mutate(id_unidade=NA) %>%
  rename(nome_unidade="Nome.do.Estabelecimento", endereco="Endereço", municipio="Município", cep="CEP", uf="UF", telefone="Telefone.Principal") %>%
  select(id_unidade, nome_unidade, endereco, cep, uf, municipio, telefone) %>%
  write.csv2(file = "unidades_2018_12.csv", na = "", fileEncoding = "UTF-8")

# Extraindo os dados referentes a unidades prisionais de jun de 2019 para um csv a parte
read.csv2("infopen_2019_06.csv", stringsAsFactors = FALSE) %>%
  mutate(id_unidade="") %>%
  rename(nome_unidade=Nome.do.Estabelecimento, endereco=Endereço, cep=CEP, uf=UF, municipio = Município, telefone=Telefone.Principal) %>%
  select(id_unidade, nome_unidade, endereco, cep, uf, municipio, telefone) %>%
  write.csv2(file="unidades_2019_06.csv", fileEncoding = "UTF-8", quote = FALSE)

# Extraindo os dados referentes a unidades prisionais de dez de 2019 para um csv a parte
read.csv2("infopen_2019_12.csv", stringsAsFactors = FALSE) %>%
  mutate(id_unidade="") %>%
  rename(nome_unidade=Nome.do.Estabelecimento, endereco=Endereço, cep=CEP, uf=UF, municipio = Município, telefone=Telefone.Principal) %>%
  select(id_unidade, nome_unidade, endereco, cep, uf, municipio, telefone) %>%
  write.csv2(file="unidades_2019_12.csv", fileEncoding = "UTF-8", quote = FALSE)
