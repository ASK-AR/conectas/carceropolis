.nao_informado_sim_nao = c("Não informado", "Sim", "Não")
.nao_informado_sim_nao_nao_se_aplica = c("Não informado", "Sim", "Não", "Não se aplica")
.orgao_lotacao_originaria = c("Não informado", "Secretaria de Administração Penitenciária ou similar", "Secretaria de Administração Pública ou similar", "Outra Secretaria Estadual", "Secretaria Municipal", "Não se aplica")

# Dataframe que contém os levels (categorias) finais esperados para cada
# variável que é do tipo "factor".
levels_finais <- list(
  sexo = c("Não informado",
           "Feminino",
           "Masculino",
           "Misto"),
  tipo_regime = c("Não informado",
                  "Estabelecimento destinado ao recolhimento de presos provisórios",
                  "Estabelecimento destinado ao cumprimento de pena em regime fechado",
                  "Estabelecimento destinado ao cumprimento de pena em regime semiaberto",
                  "Estabelecimento destinado ao cumprimento de pena em regime aberto ou de limitação de fim de semana",
                  "Estabelecimento destinado ao cumprimento de medida de segurança de internação ou tratamento ambulatorial",
                  "Estabelecimento destinado a diversos tipos de regime", "Estabelecimento destinado à realização de exames gerais e criminológico",
                  "Patronato (Estabelecimento destinado a prestar assistência aos albergados e aos egressos)",
                  "Outro"),
  tipo_estabelecimento = c("Não informado",
                           "Cadeia Pública",
                           "Centro de Detenção Provisória",
                           "Unidade de Recolhimento Provisório",
                           "Penitenciária",
                           "Colônia agrícola, industrial ou similar",
                           "Centro de Progressão Penitenciária",
                           "Unidade de Regime Semiaberto",
                           "Centro de Integração Social",
                           "Casa do albergado",
                           "Hospital de Custódia e Tratamento Psiquiátrico - HCTP", "Centro de Ressocialização",
                           "Centro de observação criminológica e triagem",
                           "Patronato",
                           "Outro"),
  gestao = c("Não informado",
             "Pública",
             "Parceria Público-Privada",
             "Co-gestão",
             "Organizações sem fins lucrativos"),
  terceirizacao_nada = c("Não informado",
                         "Nenhum serviço é terceirizado",
                         "Algum serviço é terceirizado"),
  terceirizacao_alimentacao = c("Não informado",
                                "Alimentação não é terceirizada",
                                "Alimentação é terceirizada"),
  terceirizacao_limpeza = c("Não informado",
                            "Limpeza não é terceirizada",
                            "Limpeza é terceirizada"),
  terceirizacao_lavanderia = c("Não informado",
                               "Lavanderia não é terceirizada",
                               "Lavanderia é terceirizada"),
  terceirizacao_saude = c("Não informado",
                          "Saúde não é terceirizada",
                          "Saúde é terceirizada"),
  terceirizacao_seguranca = c("Não informado",
                              "Segurança não é terceirizada",
                              "Segurança é terceirizada"),
  terceirizacao_assistencia_educacional = c("Não informado",
                                            "Assistência educacional não é terceirizada",
                                            "Assistência educacional é terceirizada"),
  terceirizacao_assistencia_laboral = c("Não informado",
                                        "Assistência laboral não é terceirizada",
                                        "Assistência laboral é terceirizada"),
  terceirizacao_assistencia_social = c("Não informado",
                                       "Assistência social não é terceirizada",
                                       "Assistência social é terceirizada"),
  terceirizacao_assistencia_juridica = c("Não informado",
                                         "Assistência jurídica não é terceirizada",
                                         "Assistência jurídica é terceirizada"),
  terceirizacao_servicos_adm = c("Não informado",
                                 "Serviços administrativos não são terceirizados",
                                 "Serviços administrativos são terceirizados"),
  terceirizacao_outro = c("Não informado",
                          "Outro serviço não é terceirizado",
                          "Outro serviço é terceirizado"),
  natureza_construcao = c("Não informado",
                          "Concebido como estabelecimento penal",
                          "Adaptado para estabelecimento penal"),
  regimento = .nao_informado_sim_nao,
  regimento_especifico = c("Não informado",
                           "Específico para o estabelecimento",
                           "Aplica-se a todos os estabelecimentos do Estado",
                           "Outra opção (especifique)"),
  cela_gestante = c("Não informado",
                    "Sim",
                    "Não",
                    "Não se aplica (estabelecimento masculino)"),
  bercario_crmi = c("Não informado",
                    "Sim",
                    "Não",
                    "Não se aplica (estabelecimento masculino)"),
  creche = c("Não informado",
             "Sim",
             "Não",
             "Não se aplica (estabelecimento masculino)"),
  consultorio_medico_disponibilidade = .nao_informado_sim_nao,
  consultorio_medico_finalidade = .nao_informado_sim_nao_nao_se_aplica,
  consultorio_odontologico_disponibilidade = .nao_informado_sim_nao,
  consultorio_odontologico_finalidade = .nao_informado_sim_nao_nao_se_aplica,
  sala_coleta_disponibilidade = .nao_informado_sim_nao,
  sala_coleta_finalidade = .nao_informado_sim_nao_nao_se_aplica,
  sala_curativo_disponibilidade = .nao_informado_sim_nao,
  sala_curativo_finalidade = .nao_informado_sim_nao_nao_se_aplica,
  cela_observacao_disponibilidade = .nao_informado_sim_nao,
  cela_observacao_finalidade = .nao_informado_sim_nao_nao_se_aplica,
  cela_enfermaria_disponibilidade = .nao_informado_sim_nao,
  cela_enfermaria_finalidade = .nao_informado_sim_nao_nao_se_aplica,
  wc_paciente_disponibilidade = .nao_informado_sim_nao,
  wc_paciente_finalidade = .nao_informado_sim_nao_nao_se_aplica,
  wc_equipe_disponibilidade = .nao_informado_sim_nao,
  wc_equipe_finalidade = .nao_informado_sim_nao_nao_se_aplica,
  farmacia_disponibilidade = .nao_informado_sim_nao,
  farmacia_finalidade = .nao_informado_sim_nao_nao_se_aplica,
  central_expurgo_disponibilidade = .nao_informado_sim_nao,
  central_expurgo_finalidade = .nao_informado_sim_nao_nao_se_aplica,
  sala_lavagem_disponibilidade = .nao_informado_sim_nao,
  sala_lavagem_finalidade = .nao_informado_sim_nao_nao_se_aplica,
  sala_esterilizacao_disponibilidade = .nao_informado_sim_nao,
  sala_esterilizacao_finalidade = .nao_informado_sim_nao_nao_se_aplica,
  vestiario_disponibilidade = .nao_informado_sim_nao,
  vestiario_finalidade = .nao_informado_sim_nao_nao_se_aplica,
  dml_disponibilidade = .nao_informado_sim_nao,
  dml_finalidade = .nao_informado_sim_nao_nao_se_aplica,
  sala_atendimento_multi_disponibilidade = .nao_informado_sim_nao,
  sala_atendimento_multi_finalidade = .nao_informado_sim_nao_nao_se_aplica,
  sala_procedimentos_disponibilidade = .nao_informado_sim_nao,
  sala_procedimentos_finalidade = .nao_informado_sim_nao_nao_se_aplica,
  sala_raiox_disponibilidade = .nao_informado_sim_nao,
  sala_raiox_finalidade = .nao_informado_sim_nao_nao_se_aplica,
  lab_diagnostico_disponibilidade = .nao_informado_sim_nao,
  lab_diagnostico_finalidade = .nao_informado_sim_nao_nao_se_aplica,
  cela_espera_disponibilidade = .nao_informado_sim_nao,
  cela_espera_finalidade = .nao_informado_sim_nao_nao_se_aplica,
  solario_disponibilidade = .nao_informado_sim_nao,
  solario_finalidade = .nao_informado_sim_nao_nao_se_aplica,
  saude_outros_disponibilidade = .nao_informado_sim_nao,
  saude_outros_finalidade = .nao_informado_sim_nao_nao_se_aplica,
  saude_outros1_finalidade = .nao_informado_sim_nao_nao_se_aplica,
  saude_outros2_finalidade = .nao_informado_sim_nao_nao_se_aplica,
  saude_outros3_finalidade = .nao_informado_sim_nao_nao_se_aplica,
  sala_aula_disponibilidade = .nao_informado_sim_nao,
  sala_informatica_disponibilidade = .nao_informado_sim_nao,
  sala_encontros_disponibilidade = .nao_informado_sim_nao,
  biblioteca_disponibilidade = .nao_informado_sim_nao,
  sala_professores_disponibilidade = .nao_informado_sim_nao,
  educacao_outros_disponibilidade = .nao_informado_sim_nao,
  modulo_oficina = .nao_informado_sim_nao,
  oficina_sala_producao = .nao_informado_sim_nao,
  oficina_sala_controle = .nao_informado_sim_nao,
  oficina_sanitarios = .nao_informado_sim_nao,
  oficina_estoque = .nao_informado_sim_nao,
  oficina_carga_descarga = .nao_informado_sim_nao,
  oficina_artefato_concreto_disponibilidade = .nao_informado_sim_nao,
  oficina_blocos_tijolos_disponibilidade = .nao_informado_sim_nao,
  oficina_padaria_disponibilidade = .nao_informado_sim_nao,
  oficina_corte_costura_disponibilidade = .nao_informado_sim_nao,
  oficina_artesanato_disponibilidade = .nao_informado_sim_nao,
  oficina_marcenaria_disponibilidade = .nao_informado_sim_nao,
  oficina_serralheria_disponibilidade = .nao_informado_sim_nao,
  local_visitacao = .nao_informado_sim_nao,
  local_visita_intima = .nao_informado_sim_nao,
  assistencia_social_sala_disponibilidade = c("Não informado",
                                              "Sim, exclusiva",
                                              "Sim, compartilhada com outros serviços",
                                              "Não"),
  assistencia_psicologica_sala_disponibilidade = c("Não informado",
                                                   "Sim, exclusiva",
                                                   "Sim, compartilhada com outros serviços",
                                                   "Não"),
  assistencia_juridica_sala_disponibilidade = c("Não informado",
                                                "Sim, exclusiva",
                                                "Sim, compartilhada com outros serviços",
                                                "Sim, parlatório",
                                                "Não"),
  sala_video_conferencia = .nao_informado_sim_nao,
  celas_seguro = .nao_informado_sim_nao,
  ala_cela_lgbt_disponibilidade = .nao_informado_sim_nao,
  ala_cela_idosos_disponibilidade = .nao_informado_sim_nao,
  ala_cela_indigenas_disponibilidade = .nao_informado_sim_nao,
  ala_cela_estrangeiros_disponibilidade = .nao_informado_sim_nao,
  ala_cela_acessibilidade_disponibilidade = .nao_informado_sim_nao,
  novos_modulos = .nao_informado_sim_nao,
  orgao_lotacao_originaria_administrativo        = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_enfermeiro            = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_auxiliar_enfermagem   = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_psicologo             = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_dentista              = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_auxiliar_odontologico = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_assistente_social     = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_advogado              = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_medico_clinico        = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_medico_ginecologista  = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_medico_psiquiatra     = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_medico_outros         = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_pedagogo              = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_professor             = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_terapeuta_ocupacional = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_policial_civil        = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_policial_militar      = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_outros                = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_servidor_outros1      = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_servidor_outros2      = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_servidor_outros3      = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_servidor_outros4      = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_servidor_outros5      = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_servidor_outros6      = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_servidor_outros7      = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_servidor_outros8      = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_servidor_outros9      = .orgao_lotacao_originaria,
  orgao_lotacao_originaria_servidor_outros10     = .orgao_lotacao_originaria,
  assistencia_juridica_gratuita_disponibilidade = c("Não informado",
                                                    "Há prestação sistemática de assistência jurídica gratuita",
                                                    "Não há prestação sistemática de assistência jurídica gratuita"),
  assistencia_juridica_gratuita_defensoria = c("Não informado",
                                               "Assistência jurídica realizada por meio da Defensoria Pública",
                                               "Não há assistência jurídica realizada por meio da Defensoria Pública"),
  assistencia_juridica_gratuita_advogados = c("Não informado",
                                              "Assistência jurídica privada prestada por advogados conveniados/dativos",
                                              "Não há assistência jurídica privada prestada por advogados conveniados/dativos"),
  assistencia_juridica_gratuita_ong = c("Não informado",
                                        "Assistência jurídica privada prestada por ONG ou outra entidade sem fins lucrativos",
                                        "Não há assistência jurídica privada prestada por ONG ou outra entidade sem fins lucrativos"),
  assistencia_juridica_gratuita_outros = c("Não informado",
                                           "Assistência jurídica gratuita realizada por outro meio",
                                           "Não há assistência jurídica gratuita realizada por outro meio"),
  controle_provisorios_90dias_informacao = .nao_informado_sim_nao,
  controle_aguardando_semiaberto_informacao = .nao_informado_sim_nao,
  atestado_pena_informacao = c("Não informado",
                               "Sim, recebe regularmente",
                               "Recebe sem regularidade",
                               "Não recebe",
                               "Não se aplica"),
  faixa_etaria_informacao = c("Não informado",
                              "Sim, para todas as pessoas privadas de liberdade",
                              "Sim, para parte das pessoas privadas de liberdade",
                              "Não"),
  raca_informacao = c("Não informado",
                      "Sim, para todas as pessoas privadas de liberdade",
                      "Sim, para parte das pessoas privadas de liberdade",
                      "Não"),
  estado_civil_informacao = c("Não informado",
                              "Sim, para todas as pessoas privadas de liberdade",
                              "Sim, para parte das pessoas privadas de liberdade",
                              "Não"),
  pessoa_deficiencia_informacao = c("Não informado",
                                    "Sim, para todas as pessoas privadas de liberdade",
                                    "Sim, para parte das pessoas privadas de liberdade",
                                    "Não"),
  tipo_penal_informacao = c("Não informado",
                            "Sim, para parte das pessoas privadas de liberdade",
                            "Sim, para todas as pessoas privadas de liberdade",
                            "Não"),
  tipo_penal_registro = c("Não informado",
                          "Apenas na inclusão do preso, sem atualização",
                          "Na inclusão do preso, atualizando-se com as informações de outros mandados de prisão ou de intimação de sentença/acórdão recebidos posteriormente",
                          "Na inclusão do preso, atualizando-se com o atestado de pena a cumprir",
                          "Não é registrada"),
  pessoas_trabalhando_informacao = .nao_informado_sim_nao,
  remuneracao_informacao = .nao_informado_sim_nao,
  auxilio_reclusao_informacao = c("Não informado",
                                  "Sim, para todas as pessoas privadas de liberdade",
                                  "Sim, para parte das pessoas privadas de liberdade",
                                  "Não"),
  auxilio_reclusao_informacao = c("Não informado",
                                  "Sim, para todas as pessoas privadas de liberdade",
                                  "Sim, para parte das pessoas privadas de liberdade",
                                  "Não"),
  visitas_informacao = c("Não informado",
                         "Sim, para todas as pessoas privadas de liberdade",
                         "Sim, para parte das pessoas privadas de liberdade",
                         "Não"),
  inspecao = .nao_informado_sim_nao,
  visita_cnpcp = c("Não informado",
                   "Recebeu visita do CNPCP",
                   "Não recebeu visita do CNPCP"),
  visita_conselho_penitenciaro = c("Não informado",
                                   "Recebeu visita do Conselho Penitenciário",
                                   "Não recebeu visita do Conselho Penitenciário"),
  visita_conselho_comunidade = c("Não informado",
                                 "Recebeu visita do Conselho da Comunidade",
                                 "Não recebeu visita do Conselho da Comunidade"),
  visita_ouvidoria = c("Não informado",
                       "Recebeu visita da Ouvidoria do sistema prisional",
                       "Não recebeu visita da Ouvidoria do sistema prisional"),
  visita_defensoria = c("Não informado",
                        "Recebeu visita da Defensoria Pública",
                        "Não recebeu visita da Defensoria Pública"),
  visita_judiciario = c("Não informado",
                        "Recebeu visita do Judiciário",
                        "Não recebeu visita do Judiciário"),
  visita_ministerio_publico = c("Não informado",
                                "Recebeu visita do Ministério Público",
                                "Não recebeu visita do Ministério Público"),
  visita_outros = c("Não informado",
                    "Recebeu visita de Outros órgãos",
                    "Não recebeu visita de Outros órgãos"),
  assistencia_social_sala_disponibilidade = c("Não informado",
                                              "Sim, exclusiva",
                                              "Sim, compartilhada com outros serviços",
                                              "Não"),
  assistencia_psicologica_sala_disponibilidade = c("Não informado",
                                                   "Sim, exclusiva",
                                                   "Sim, compartilhada com outros serviços",
                                                   "Não")
)
