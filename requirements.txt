# Mantendo mezzanine como primeiro item porque ele depende de uma versão bem
# antiga do django
git+https://github.com/diraol/filebrowser-safe@2.2-compat#egg=filebrowser_safe
git+https://github.com/diraol/mezzanine@2.2-compat#egg=mezzanine
# mezzanine
bokeh
django_compressor
django-extensions
django-libsass
django-logentry-admin
django-modeltranslation
django-phonenumber-field
flexx
geocoder==1.33.0
ipython
numpy
pandas
psycopg2-binary==2.8.6
python-memcached
tornado
unidecode
uwsgi
